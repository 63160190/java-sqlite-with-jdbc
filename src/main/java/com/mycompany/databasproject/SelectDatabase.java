/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.databasproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author User
 */
public class SelectDatabase {
    public static void main(String[] args) {
        
        Connection conn = null;
        String url = "jdbc:sqlit:dcoffee.db";
        
        try {
            conn = DriverManager.getConnection(url);
            System.out.println("Connection to SQLite has benn establish.");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return;
        } 
        //seclection
           String sql = "SELECT * FROM Order";
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            
            while(rs.next()){
                System.out.println(rs.getInt("order_id")+" "
                        +rs.getInt("store_id"));
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(SelectDatabase.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        
        
        
    }
}
